import { Link, Routes, Route } from "react-router-dom";

import "./App.css";

import { Layout, Menu } from "antd";
import Home from "./pages/Home";
import Posts from "./pages/Posts";

const { Header, Content, Footer } = Layout;

const navItems = [
  { label: "Home", key: "home", path: "/" },
  { label: "Posts", key: "posts", path: "/posts" },
];

function App() {
  return (
    <div className="App">
      <Layout className="layout">
        <Header>
          <div className="logo" />
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={["0"]}
            items={navItems.map((item) => {
              return {
                key: item.key,
                label: (
                  <Link to={item.path} title={item.label}>
                    {item.label}
                  </Link>
                ),
              };
            })}
          />
        </Header>

        <Content
          style={{
            padding: "0 50px",
          }}
        >
          <div className="site-layout-content">
            <Routes>
              <Route path="/about" element="" />
              <Route path="/posts" element={<Posts />} />
              <Route path="/" element={<Home />} />
            </Routes>
          </div>
        </Content>

        <Footer
          style={{
            textAlign: "center",
          }}
        >
          My blog with React
        </Footer>
      </Layout>
    </div>
  );
}

export default App;
