import axios from "axios";

const COSMIC_BUCKET_SLUG = process.env.REACT_APP_COSMIC_BUCKET_SLUG || "";
const COSMIC_READ_KEY = process.env.REACT_APP_COSMIC_READ_KEY || "";
const COSMIC_API_URL = process.env.REACT_APP_COSMIC_API_URL || "";
const BASE_URL = `${COSMIC_API_URL}/${COSMIC_BUCKET_SLUG}`;

const connection = axios.create({
  baseURL: BASE_URL,
});

export const getPosts = async () => {
  const response = await connection.get("/objects", {
    params: {
      query: { type: "posts" },
      limit: 20,
      read_key: COSMIC_READ_KEY,
      props: "id,slug,title,content",
    },
  });

  return response.data;
};
