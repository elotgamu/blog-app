import * as React from "react";

import { Spin } from "antd";

import PostsList from "../../components/PostsList";
import { getPosts } from "../../services/CosmicAPI";

const Posts = () => {
  const [data, setData] = React.useState({
    loading: false,
    error: null,
    posts: null,
  });

  const fetchPosts = async () => {
    try {
      setData((d) => ({ ...d, loading: true }));
      const data = await getPosts();
      setData((d) => ({
        ...d,
        posts: data.objects,
        loading: false,
        error: null,
      }));
    } catch (error) {
      setData((d) => ({ ...d, error: error, loading: false }));
    }
  };

  React.useEffect(() => {
    fetchPosts();
  }, []);

  return (
    <main>
      <section>
        <h2>Posts: </h2>
        {data.loading && <Spin size="large" />}
      </section>

      {data.error ? (
        <section>
          <p>
            An error ocurred:{" "}
            {data.error.message ? data.error.message : data.error}
          </p>
        </section>
      ) : null}

      {data.posts ? (
        <section>
          <PostsList posts={data.posts} />
        </section>
      ) : null}
    </main>
  );
};

export default Posts;
