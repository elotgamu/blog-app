const PostsList = ({ posts = [] }) => {
  return (
    <div>
      {!posts?.length ? <p>No Posts to display</p> : null}
      {posts && posts.length ? (
        <ul>
          {posts.map((post) => (
            <li key={post.id}>{post.title}</li>
          ))}
        </ul>
      ) : null}
    </div>
  );
};

export default PostsList;
